import jwt from 'jsonwebtoken';

//Repasar código

const isAuth = (req, res, next) => {
    
    if (!authoritation) {
        return res.status(401).json({
            status: 401,
            message: "UnauTHORized",
            data: null,
        })
    }

    const [bearerString, jwtString] = authoritation.split(" ")
    
    if (bearerString !== "Bearer") {
        return res.status(400).json({
            status: 400,
            message: "BAT Request",
            data: null,
        })
    }
    try {
        var token = jwt.verify(jwtString, req.app.get("secretKey"));
    } catch (error) {
        return next(error)
    }
    const authority = {
        id: token.id,
        email: token.email,
        rol: token.rol
    }

    req.authority = authority;
    next();
}

export { isAuth }