import express from "express";

import 'dotenv/config';
import { connection } from "./config/database.js";
import cors from "cors"
import logger from 'morgan';
import { userRoutes } from './api/routes/user.routes.js'
import { terminalRoutes } from "./api/routes/terminales.routes.js";
import { flotaRoutes } from "./api/routes/flota.routes.js";
connection();


const PORT = process.env.PORT;
const router = express.Router();
const server = express();

server.set("secretKey", "nodeRestApi");

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

server.use(logger('dev'));
server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(cors("*"));

server.use('/', router);
server.use("/users", userRoutes);
server.use('/terminales', terminalRoutes)
server.use('/flota', flotaRoutes)



const serverListen = server.listen(PORT, () => {
    console.log(`Node server linstening on port ${PORT}`)
});
