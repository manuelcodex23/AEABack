import  mongoose  from "mongoose"

const Schema = mongoose.Schema;

const flotaSchema = new Schema(
    {
        typeName: { type: String, required: true },
        matriculas: { type: Array, required: true },
        pasajeros: { type: String, required: true },
        wheelColor: { type: String },
        image:{type:String}
    },
    {
        timestamps: true,
    }
);

const Flota = mongoose.model('Flota', flotaSchema)

export {Flota}
