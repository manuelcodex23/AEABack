import mongoose from "mongoose"

const Schema = mongoose.Schema;

const terminalesSchema = new Schema(
    {
        name: { type: String, required: true },
        logo: { type: String, required: true },
        doc: { type: Array, required: true },
        acceso: { type: String, required: true },
        image: { type: String }
    },
    {
        timestamps: true,
    }
);

const Terminal = mongoose.model('Terminal', terminalesSchema)

export {Terminal}