import { Flota } from "../models/flota.model.js";

import { httpStatusCode } from "../../utils/httpStatusCode.js";

const getAllFlota = async (req, res, next) => {
    try {
        const flota = await Flota.find().populate();
        return res.status(200).json(flota);
    } catch (error) {
        return next(error);
    }
}

const createFlota = async (req, res, next) => {
    try {
        const { body } = req;
        const newFlota = new Flota({
            typeName : body.typeName,
            matriculas : body.matriculas,
            pasajeros : body.pasajeros,
            wheelColor: body.wheelColor,
            image: body.image
        })
        const savedFlota = await newFlota.save();
        return res.json({
            status: 201,
            message: httpStatusCode[201],
            data: savedFlota
        })
    } catch (error) {
        return next(error)
    }
}
const getFlotaById = async(req, res, next)=>{
    try {
        const { id } = req.params;

        const flotaById = await Flota.findById(id);
        return res.status(200).json(flotaById)
    } catch (error) {
        return next(error)
    }
}

const deleteFlota = async (req, res, next)=>{
    try {
        const { id } = req.params;
        const flotaDelete = await Flota.findByIdAndDelete(id);
        return res.json({
            status: 200,
            messagw: "Flota menguada",
            data: { flota: flotaDelete }
        });
    } catch (error) {
        return next(error);
    }
}

export { getAllFlota, createFlota, getFlotaById, deleteFlota }