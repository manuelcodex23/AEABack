import { User } from "../models/user.model.js";

import bcrypt from "bcrypt";

import jwt from "jsonwebtoken";

import { httpStatusCode } from "../../utils/httpStatusCode.js";

const getAllUsers = async (req, res, next) => {
  try {
    const users = await User.find().populate("name");
    return res.status(200).json(users);
  } catch (error) {
    return next(error);
  }
};

const createUser = async (req, res, next) => {
  try {
    const { body } = req;
    //Consultar los  users registrados
    const previousUser = await User.findOne({ email: body.email });

    if (previousUser) {
      const error = new Error("The user is already registered!");
      return next(error);
    }

    // Encriptar password
      const pwdHash = await bcrypt.hash(body.password, 10);
      
    const newUser = new User({
    name : body.name,
    email : body.email,
    numero_empleado : body.numero_empleado,
    password : pwdHash
});
    const userDb = await newUser.save();

    return res.json({
      status: 201,
      message: httpStatusCode[201],
      data: userDb._id
    });
  } catch (err) {
    return next(err);
  }
};

const loginUser = async (req, res, next) => {
    try {
        const { body } = req;
    
        // Comprobar email
        const user = await User.findOne({ email: body.email });
    
        // Comprobar password
        const isValidPassword = await bcrypt.compare(body.password, user?.password ?? '');
        // Control de LOGIN
        if (!user || !isValidPassword) {
          const error = {
            status: 401,
            message: 'The email & password combination is incorrect!'
          };
          return next(error);
        }

    const token = jwt.sign(
      {
        id: userInfo._id,
        email: userInfo.email,
        numero: userInfo.numero_empleado,
      },
      req.app.get("secretKey"),
      { expiresIn: "3h" }
    );
    return res.json({
      status: 200,
      message: httpStatusCode[200],
      data: {
        user: userInfo._id,
        email: userInfo.email,
        token: token,
        numero: userInfo.numero_empleado,
      },
    });
  } catch (error) {
    return next(error);
  }
};

const logoutUser = async (req, res, next) => {
  try {
    req.authority = null;
    return res.json({
      status: 200,
      message: "logged out",
      token: null,
    });
  } catch (error) {
    next(error);
  }
};

const editUser = async (req, res, next) => {
  const userPhoto = req.file_url;
  const bodyData = req.body;

  if (userPhoto) {
    bodyData.image = userPhoto;
  }
  const { id: userId } = req.authority;
  try {
    const user = await User.findById(userId);
    const usermodify = new User(bodyData);
    usermodify._id = userId;
    await User.findByIdAndUpdate(userId, usermodify);
    return res.json({
      status: 200,
      message: httpStatusCode[200],
      data: { user: usermodify },
    });
  } catch (error) {
    return next(error);
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    const userDelete = await User.findByIdAndDelete(id);
    return res.json({
      status: 200,
      message: httpStatusCode[200],
      data: { user: userDelete },
    });
  } catch (error) {
    return next(error);
  }
};

export { createUser, loginUser, logoutUser, editUser, deleteUser, getAllUsers };
