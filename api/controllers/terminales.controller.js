import { Terminal } from "../models/terminales.model.js";

import { httpStatusCode } from "../../utils/httpStatusCode.js";

const getAllTerminal = async (req, res, next) => {
    try {
        const terminales = await Terminal.find().populate();
        return res.status(200).json(terminales);
    } catch (error) {
        return next(error);
    }
};

const createTerminal = async (req, res, next) => {
    try {
        const { body } = req;
        const newTerminal = new Terminal({
            name: body.name,
            logo: body.logo,
            doc: body.doc,
            acceso: body.acceso,
            image: body.images
        })
        const savedTerminal = await newTerminal.save();
        return res.json({
            status: 201,
            message: httpStatusCode[201],
            data: savedTerminal
        })
    } catch (error){
        return next(error)
    }
}

const getTerminalById = async (req, res, next) => {
    try {
        const { id } = req.params;

        const terminalById = await Terminal.findById(id);
        return res.status(200).json(terminalById)
    } catch (error) {
        return next(error)
    }
}

const deleteTerminal = async (req, res, next) => {
    try{
        const { id } = req.params;
        const terminalDelete = await Terminal.findByIdAndDelete(id);
        return res.json({
            status: 200,
            message: "Terminal Delete",
            data: { terminal: terminalDelete }
        });
    } catch (error) {
        return next(error);
    }
}


export {getAllTerminal, createTerminal, getTerminalById, deleteTerminal}