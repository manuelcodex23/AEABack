import express from "express";
import { getAllTerminal, createTerminal, getTerminalById, deleteTerminal } from "../controllers/terminales.controller.js";
import { upload, uploadToCloudinary } from "../../middlewares/file.middleware.js";

const terminalRoutes = express.Router();

terminalRoutes.get('/', getAllTerminal);
terminalRoutes.put('/create', [upload.single('image'), uploadToCloudinary], createTerminal);
terminalRoutes.get('/:id', getTerminalById);
terminalRoutes.delete('/:id', deleteTerminal);

export { terminalRoutes };