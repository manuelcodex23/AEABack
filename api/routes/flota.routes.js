import express from "express";
import { getAllFlota, createFlota, getFlotaById, deleteFlota } from "../controllers/flota.controller.js";
import { upload, uploadToCloudinary } from "../../middlewares/file.middleware.js";

const flotaRoutes = express.Router();

flotaRoutes.get('/', getAllFlota);
flotaRoutes.put('/create', [upload.single('image'), uploadToCloudinary], createFlota);
flotaRoutes.get('/:id', getFlotaById);
flotaRoutes.delete('/:id', deleteFlota);

export {flotaRoutes}