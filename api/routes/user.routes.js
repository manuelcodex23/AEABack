import  express from "express";

import { getAllUsers, createUser, loginUser, logoutUser, editUser, deleteUser } from '../controllers/user.controller.js'
import { upload, uploadToCloudinary } from '../../middlewares/file.middleware.js';
import {isAuth} from '../../authentication/jwt.js'
const userRoutes = express.Router();

userRoutes.get('/', getAllUsers )
userRoutes.post('/create', createUser);
userRoutes.post('/login', loginUser);
userRoutes.post('/logout', logoutUser);
userRoutes.put('/edit', [isAuth, upload.single('image'), uploadToCloudinary], editUser)
userRoutes.delete('/delete-user', deleteUser);

export { userRoutes };